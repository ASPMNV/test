from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
from datetime import datetime
import os

port = int(os.environ.get("PORT", 80))


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()


class HttpGetHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write('<html><head><meta charset="utf-8">'.encode())
        self.wfile.write('<title>HTTP-сервер заряжен и стреляет.</title></head>'.encode())
        self.wfile.write(f'<body><span>{datetime.now().strftime("%d-%m-%Y %H:%M")}&nbspПушка  стреляет</span></body></html>'.encode())


if __name__ == '__main__':
    run(handler_class=HttpGetHandler)
