#syntax=docker/dockerfile:1
FROM alpine:latest

RUN apk add --no-cache --update python3

WORKDIR /app

COPY . .

CMD ["python3", "main.py", "$PORT"]
